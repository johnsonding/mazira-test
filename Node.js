/*==========================================================================================
 *
 *  Test: Kelvin to Fahrenheit Converter Mudule and Current Temperature
 *
 *  Date: September 26, 2014
 *
 *  Author: Johnson Ding
 *
 *  Description: This node module contains two functions:
 *
 *               fahrenheitFromKelvin: this function will convert Kelvin to Fahrenheit 
 *                                     with user input for Kelvin on command line.
 *
 *               fahrenheitFromCity: this function will show the current temperature of a
 *                                   location (city) from user's input.
 *
 *  Example: 
 *  
 *  fahrenheitFromKelvin:   (convert 1 Kelvin to Fahrenheit)
 *
 *  Which function would you like to use? 1. fahrenheitFromKelvin 2. fahrenheitFromCity 
 *  1
 *  Please enter degree in Kelvin
 *  1
 *  1 K = -457.86999999999995 F
 *
 *  fahrenheitFromCity: (determine current temperature at Coralville)
 *
 *  Which function would you like to use? 1. fahrenheitFromKelvin 2. fahrenheitFromCity
 *  2
 *  Please enter the desired location
 *  Coralville
 *  The current temperature at Coralville is 65 F.
 *
 *==========================================================================================*/

var readline = require('readline'); //scan for user input
var rl = readline.createInterface({ input: process.stdin , output: process.stdout });
//var rlfun1 = readline.createInterface({ input: process.stdin , output: process.stdout });
var userInput = 0;                  //user selection for function

rl.question("Which function would you like to use? 1. fahrenheitFromKelvin 2. fahrenheitFromCity \n"
            , function(answer) {    //ask user which function to use
            
    //console.log('check1',answer);        //debug

    if (answer == 1){       //check if user wants to do fahrenheitFromKelvin

        userInput = 1;      //assign user selection to function 1
            
        //console.log('cool',userInput);    //debug

    }

    if (answer == 2){       //check if user wants to do fahrenheitFromCity

        userInput = 2;      //assign user selection to function 2
        
        //console.log('cool',userInput);   //debug

    }

    if (answer != 1 && answer != 2){    //unvalid input

        console.log('Error! Please select again with input of only "1" or "2".');
        rl.close();
        return -1;

    }

            
    //console.log(userInput);   //debug

    //rl.close();


    function fahrenheitFromKelvin(kelvin){      //fahrenheitFromKelvin function

        return((kelvin - 273.15)* 1.8000 + 32.00);   //conversion formula

    }


    if (userInput == 1) {   //user selects fahrenheitFromKelvin

        rl.question("Please enter degree in Kelvin \n", function(answer) {
        //scan user input for temperature in Kelvin
                  
            //console.log(answer);        //debug
                    
            console.log('%d K = %d F', answer, fahrenheitFromKelvin(answer) );    //print result
            
            rl.close();
                
        });

    }

    if (userInput == 2) {   //user selects fahrenheitFromKelvin
    
        rl.question("Please enter the desired location \n", function(answer) {
        //scan user input for temperature in Kelvin
            
            console.log(answer);        //debug
            
            //console.log('%d K = %d F', answer, fahrenheitFromKelvin(answer) );    //print result
            
            //rl.close();
                    
                    
            
            
            var weather = require('weather');
            
            weather({location: 'Melbourne'}, function(data) {
            //weather API: https://www.npmjs.org/package/weather
            //$ npm install weather
                    
                console.log('The current temperature at %d is %d F.\n', data.temp, answer);
                //print out desired location and its current temperature
                
            });
            
            rl.close(); //close user input scanning
                    
        });
    
    }
            
           

});






